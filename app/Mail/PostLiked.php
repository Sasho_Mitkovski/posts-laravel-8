<?php

namespace App\Mail;

use App\Models\Post;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PostLiked extends Mailable
{
    use Queueable, SerializesModels;

    public $poster;
    public $post;
    public function __construct(User $poster, Post $post)
    {
        $this->poster = $poster;
        $this->post = $post;
     }

    public function build()
    {
        return $this->markdown('emails.posts.post_liked')
        ->subject('Ova e od Like');
    }
}
