@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12">
            <div class="p-6">
                <h1 class="text-2xl font-medium mb-1">{{ $user->name }}</h1>
                <p>Posted {{ $posts->count() }} {{ Str::plural('post', $posts->count()) }} and received {{ $user->receivedLikes->count() }} likes</p>
            </div>
            <div class="bg-white p-6 rounded-lg">
                @if ($posts->count())
                @foreach ($posts as $post)
                    <div class="mb-4">
                        {{-- link na imeto koj ke ne nosi na post od toj admin --}}
                        <a href="{{ route('posts.show', $post) }}" class="font-bold">{{ $post->user->name }}</a><span class="text-gray-600 text-sm">
                            {{ $post->created_at->diffForHumans() }}</span>
                        <p class="mb-4"> {{ $post->body }}</p>
                        {{-- Brisenje na Post --}}

                      @can('delete', $post)
                            <form action="{{ route('posts.destroy', $post) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="text-red-500 ">Delete</button>
                            </form>
                    @endcan
                        <div class="flex items-center">
                            @auth
                            @if (!$post->likedBy(auth()->user()))
                            {{-- Like --}}
                                <form action="{{ route('posts.likes', $post) }}" method="post" class="mr-1">@csrf
                                    <button type="submit" class="text-blue-500">Like</button>
                                </form>
                            @else
                            {{-- UnLike --}}
                                <form action="{{ route('posts.likes', $post) }}" method="post" class="mr-1">@csrf
                                    @method('DELETE')
                                    <button type="submit" class="text-blue-500">Unlike</button>
                                </form>
                                {{-- Pokazuvanje na vkupno lajkovi --}}
                                <span>{{ $post->likes->count() }} {{ Str::plural('like', $post->likes->count()) }}</span>
                            @endif
                            @endauth
                        </div>
                    </div>
                @endforeach
                {{-- Paginate --}}
                {{ $posts->links() }}
            @else
                <p>There are no posts</p>
            @endif
            </div>
        </div>
    </div>
@endsection
