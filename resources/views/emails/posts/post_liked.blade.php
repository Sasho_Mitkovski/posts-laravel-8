@component('mail::message')
# Introduction

The body of your message.

@component('mail::button', ['url' => route('posts.show', $post)])
View post
@endcomponent

Thanks,<br>
{{ auth()->user()->name}}
@endcomponent
