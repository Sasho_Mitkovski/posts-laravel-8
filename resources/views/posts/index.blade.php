@extends('layouts.app')
@section('content')
    <div class="flex justify-center mt-6">
        <div class="w-8/12 bg-white p-6 rounded-lg ">
            <form action="{{ route('posts') }}" method="POST" class="mb-4">
                @csrf
                <label for="body" class="sr-only">Body</label>
                <textarea name="body" id="body" cols="30" rows="4" class="
            bg-gray-100 border-2 w-full p-4 rounded-lg @error('body') border-red-500 @enderror"
                    placeholder="Post someting!"></textarea>
                @error('body')
                    {{ $message }}
                @enderror
                <div>
                    <button type="submit" class="bg-blue-500 text-white px-4 py-3 mt-4
                                rounded font-medium">Post</button>
                </div>
            </form>
            @if ($posts->count())
                @foreach ($posts as $post)
                    <div class="mb-4">
                        {{-- link na imeto koj ke ne nosi na post od toj admin --}}
                        <a href="{{ route('users.posts', $post->user) }}" class="font-bold">{{ $post->user->name }}</a><span class="text-gray-600 text-sm">
                            {{ $post->created_at->diffForHumans() }}</span>
                        <p class="mb-4"> {{ $post->body }}</p>
                        {{-- Brisenje na Post --}}

                       @can('delete', $post)
                            <form action="{{ route('posts.destroy', $post) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="text-red-500 ">Delete</button>
                            </form>
                      @endcan

                        <div class="flex items-center">
                            @auth
                            @if (!$post->likedBy(auth()->user()))
                            {{-- Like --}}
                                <form action="{{ route('posts.likes', $post) }}" method="post" class="mr-1">@csrf
                                    <button type="submit" class="text-blue-500">Like</button>
                                </form>
                            @else
                            {{-- UnLike --}}
                                <form action="{{ route('posts.likes', $post) }}" method="post" class="mr-1">@csrf
                                    @method('DELETE')
                                    <button type="submit" class="text-blue-500">Unlike</button>
                                </form>
                                {{-- Pokazuvanje na vkupno lajkovi --}}
                                <span>{{ $post->likes->count() }} {{ Str::plural('like', $post->likes->count()) }}</span>
                            @endif
                            @endauth
                        </div>
                    </div>
                @endforeach
                {{-- Paginate --}}
                {{ $posts->links() }}
            @else
                <p>There are no posts</p>
            @endif
        </div>
    </div>
@endsection
